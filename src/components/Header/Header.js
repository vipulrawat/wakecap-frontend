import React from 'react';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import NewReport from '../NewReport/NewReport';
import { logoutUser } from '../../services/user';

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  subheading: {
    marginRight: 20
  }
};

function handleLogoutBtnClick(logout) {
  logoutUser(); // Removes the token from storage
  logout(); // Props logout to go back
}

const ButtonAppBar = props => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h5" color="inherit" className={classes.grow}>
            <a href="/" style={{ color: 'white', textDecoration: 'none' }}>
              WakeCap
            </a>
          </Typography>
          <Typography
            variant="subtitle2"
            color="inherit"
            className={classes.subheading}
          >
            Welcome, {window.localStorage.getItem('role')}
          </Typography>
          <Button
            color="inherit"
            variant="outlined"
            onClick={() => handleLogoutBtnClick(props.logout)}
          >
            Logout
          </Button>
          <NewReport />
        </Toolbar>
      </AppBar>
    </div>
  );
};

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  logout: PropTypes.func
};

ButtonAppBar.defaultProps = {
  logout: () => {}
};

export default withStyles(styles)(ButtonAppBar);
