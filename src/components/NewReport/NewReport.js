import React from 'react';
import CreateIcon from '@material-ui/icons/NoteAdd';
import { Fab, Dialog, DialogContent, DialogTitle } from '@material-ui/core';

import Form from './Form';

class FormDialog extends React.Component {
  state = {
    open: false
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const btnStyles = {
      position: 'fixed',
      bottom: '30px',
      right: '30px'
    };

    return (
      <div>
        <Fab
          variant="extended"
          color="primary"
          style={btnStyles}
          onClick={this.handleClickOpen}
        >
          <CreateIcon style={{ margin: '10px' }} />
          Create Report
        </Fab>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            Create new inspection report
          </DialogTitle>
          <DialogContent>
            <Form onSubmit={this.handleClose} />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default FormDialog;
