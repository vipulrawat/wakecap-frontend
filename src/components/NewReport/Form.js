import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Input,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Button,
  TextField
} from '@material-ui/core';
import { createReport } from '../../services/report';

const styles = theme => ({
  root: {
    display: 'block',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  error: {
    color: 'red'
  }
});

class Form extends Component {
  state = {
    projectName: 'IN1',
    floorNumber: 1,
    zoneNumber: 1,
    title: '',
    body: '',
    error: null,
    role: window.localStorage.getItem('role')
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleClick = async event => {
    // event.preventDefault();
    try {
      const { error, ...report } = this.state;
      await createReport(report);
      this.props.onSubmit(); // closes the dialog
    } catch (e) {
      this.setState({ error: e.name });
    }
  };

  render() {
    const { classes } = this.props;
    const { error } = this.state;
    if (error)
      return (
        <h1 className={classes.error}>
          {error === 'AuthError'
            ? 'Only SO can create new inspection report.'
            : error}
        </h1>
      );
    return (
      <form
        className={classes.root}
        autoComplete="off"
        onSubmit={this.handleClick}
      >
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="projectName">Project Name</InputLabel>
          <Select
            value={this.state.projectName}
            onChange={this.handleChange}
            inputProps={{
              name: 'projectName',
              id: 'projectName'
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={'IN1'}>IN1</MenuItem>
            <MenuItem value={'IN2'}>IN2</MenuItem>
            <MenuItem value={'IN3'}>IN3</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="floorNumber">Floor Number</InputLabel>
          <Select
            value={this.state.floorNumber}
            onChange={this.handleChange}
            inputProps={{
              name: 'floorNumber',
              id: 'floorNumber'
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="zoneNumber">Zone Number</InputLabel>
          <Select
            value={this.state.zoneNumber}
            onChange={this.handleChange}
            inputProps={{
              name: 'zoneNumber',
              id: 'zoneNumber'
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
          </Select>
        </FormControl>
        {/*  */}
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="title">Title</InputLabel>
          <Input
            required
            placeholder="Title"
            className={classes.input}
            onChange={this.handleChange}
            inputProps={{
              name: 'title',
              id: 'title'
            }}
          />
        </FormControl>
        <FormControl className={classes.formControl}>
          <TextField
            required
            multiline
            rows="4"
            placeholder="Description"
            onChange={this.handleChange}
            inputProps={{
              name: 'body',
              id: 'body'
            }}
          />
        </FormControl>
        <FormControl className={classes.formControl}>
          {this.state.role === 'so' ? (
            <Button type="Submit" color="primary" variant="contained">
              Create
            </Button>
          ) : (
            <Button
              type="Submit"
              color="secondary"
              variant="contained"
              disabled
            >
              Not Allowed
            </Button>
          )}
        </FormControl>
      </form>
    );
  }
}
export default withStyles(styles)(Form);
