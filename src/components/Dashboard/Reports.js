import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

import ReportCards from './ReportCards';
import ReportDetails from './ReportDetails';

const styles = {
  root: {},
  paper: {},
  card: {
    maxWidth: 345
  },
  media: {
    objectFit: 'cover'
  },
  status: {
    background: '#03DAC6',
    height: 20,
    marginLeft: 20
  }
};

class ReportCard extends React.Component {
  state = {
    open: false,
    report: {}
  };
  handleClickOpen = report => {
    this.setState({ open: true, report });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, reports } = this.props;
    return reports.map(report => (
      <React.Fragment key={report._id}>
        <Grid item xm='true'>
          <ReportCards
            classes={classes}
            report={report}
            handleClickOpen={this.handleClickOpen}
          />
        </Grid>
        <ReportDetails
          handleClose={this.handleClose}
          open={this.state.open}
          report={this.state.report}
        />
      </React.Fragment>
    ));
  }
}

ReportCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ReportCard);
