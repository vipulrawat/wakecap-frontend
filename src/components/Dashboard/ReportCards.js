import React from 'react';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Chip,
  Typography
} from '@material-ui/core';

const ReportCards = props => {
  const { classes, report } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea onClick={() => props.handleClickOpen(report)}>
        <CardMedia
          component="img"
          alt="Report Image"
          className={classes.media}
          height="140"
          image="/placeholder.jpg"
          title="Report Image"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {report.title}
            <Chip label={report.status} className={classes.status} />
            <Typography variant="caption" gutterBottom>
              Project: {report.projectName} | Floor: {report.floorNumber} |
              Zone: {report.zoneNumber}
            </Typography>
          </Typography>
          <Typography component="p">{report.body}</Typography>
          <hr />
          <Typography variant="caption" gutterBottom>
            Created At: {report.createdAt}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default ReportCards;
