import React from 'react';
import { Dialog, DialogContent } from '@material-ui/core';
import ReportContent from './ReportContent';

class ReportDetails extends React.Component {
  state = {
    role: window.localStorage.getItem('role')
  };
  render() {
    const { open, handleClose, report } = this.props;
    return (
      <Dialog open={open} onClose={handleClose}>
        <DialogContent>
          <ReportContent report={report} />
        </DialogContent>
      </Dialog>
    );
  }
}

export default ReportDetails;
