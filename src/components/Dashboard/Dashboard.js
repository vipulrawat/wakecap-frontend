import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { getReports } from '../../services/report';
import { getStatus } from '../../services/status';
import Reports from './Reports';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 20,
    marginLeft: 50
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary
  }
});

class Dashboard extends React.Component {
  state = {
    reports: [],
    error: ''
  };
  componentDidMount() {
    this.refreshReports();
  }

  async refreshReports() {
    let reports = [];
    try {
      let r = await getReports();
      let s = await getStatus();
      r.forEach((itm, i) => {
        reports.push(Object.assign({}, itm, s[i]));
      });
    } catch (e) {
      if (e.name === 'AuthError') {
        this.setState({
          error: 'Authentication failure. Please log out and log in again.'
        });
      } else {
        this.setState({ error: e.message });
      }
      return;
    }
    this.setState({ reports });
  }
  render() {
    const { classes } = this.props;
    const { reports } = this.state;
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Reports reports={reports} classes={classes} />
        </Grid>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Dashboard);
