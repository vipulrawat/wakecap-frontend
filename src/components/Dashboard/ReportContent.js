import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import red from '@material-ui/core/colors/red';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Collapse,
  Avatar,
  IconButton,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button
} from '@material-ui/core';
import { editStatus } from '../../services/status';
const styles = theme => ({
  card: {
    minWidth: 400,
    maxWidth: '100%'
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  actions: {
    display: 'flex'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: red[500]
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: '100%'
  }
});

class ReportContent extends React.Component {
  constructor(props) {
    super(props);
    let role = window.localStorage.getItem('role');
    this.allowed = [];
    if (role === 'sm') {
      this.allowed.push('Assigned', 'Reassigned', 'Completed');
    } else if (role === 'se') {
      this.allowed.push('Resolved');
    } else {
      this.allowed.push('Opened');
    }
  }
  state = { expanded: false, status: '' };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleClick = async (event, id) => {
    // event.preventDefault();
    try {
      console.log(id);
      const newStatus = this.state.status;
      await editStatus({ newStatus }, id);
      // this.props.onSubmit();  // closes the dialog
    } catch (e) {
      this.setState({ error: e.name });
    }
  };

  render() {
    const { classes, report } = this.props;

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="projectName" className={classes.avatar}>
              {report.projectName}
            </Avatar>
          }
          title={report.title}
          subheader={new Date(report.createdAt).toDateString()}
        />
        <CardMedia
          className={classes.media}
          image="/placeholder.jpg"
          title="Report Image"
        />
        <CardContent>
          <Typography component="h3">
            <strong>Description:</strong><br/>
            {report.body}
          </Typography>
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Current Status: {report.status}</Typography>
            <form
              className={classes.root}
              autoComplete="off"
              onSubmit={e => this.handleClick(e, report._id)}
            >
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="status">Status</InputLabel>
                <Select
                  value={this.state.status}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'status',
                    id: 'status'
                  }}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {this.allowed.map((item, idx) => (
                    <MenuItem value={item} key={idx}>{item}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl className={classes.formControl}>
                <Button type="Submit" color="primary" variant="contained">
                  Update
                </Button>
              </FormControl>
            </form>
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}

ReportContent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ReportContent);
