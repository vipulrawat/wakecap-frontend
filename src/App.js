import React, { Component } from 'react';
import { SignInComponent, Header, Dashboard } from './components';
import { checkLoggedIn } from './services/user';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: checkLoggedIn()
    };
  }
  setLoggedIn = () => this.setState({ loggedIn: true });
  setLoggedOut = () => this.setState({ loggedIn: false });
  render() {
    const { loggedIn } = this.state;
    return (
      <div className="App">
        {!loggedIn ? (
          <SignInComponent login={this.setLoggedIn} />
        ) : (
          <React.Fragment>
            <Header logout={this.setLoggedOut} />
            <Dashboard />
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default App;
