import { get, put } from './http';

export async function getStatus() {
  const status = await get('/api/status', null, true);
  return status;
}

export async function getStatusById(id) {
  const status = await get(`/api/status/${id}`, null, true);
  return status;
}

export async function editStatus(payload, id) {
  await put(`/api/status/${id}`, { ...payload }, true);
}
