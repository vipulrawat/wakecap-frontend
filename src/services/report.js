import { get, post } from './http';

export async function getReports() {
  const reports = await get('/api/report', null, true);
  return reports;
}

export async function getReportById(reportId) {
  const report = await get(`/api/report/${reportId}`, null, true);
  return report;
}

export async function createReport(payload) {
  await post('/api/report', { ...payload }, true);
}
