import { post } from './http';

export async function loginUser(payload) {
  const { token, role } = await post('/users/login', payload);
  window.localStorage.setItem('token', token);
  window.localStorage.setItem('role', role);
}

export function checkLoggedIn() {
  return window.localStorage.getItem('token') !== null;
}

export function logoutUser() {
  window.localStorage.removeItem('token');
  window.localStorage.removeItem('role');
}
